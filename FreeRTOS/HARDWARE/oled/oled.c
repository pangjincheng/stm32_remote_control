#include "oled.h"
#include "stdlib.h"
#include "oledfont.h"  	 
#include "delay.h"
#include "font.h"

u8 OLED_GRAM[144][8];
u8 OLED_UPDATE[128];


//反显函数
void OLED_ColorTurn(u8 i)
{
	if(i==0)
		OLED_WR_Byte(0xA6,OLED_CMD);//正常显示
	else
		OLED_WR_Byte(0xA7,OLED_CMD);//反色显示
}

//屏幕旋转180度
void OLED_DisplayTurn(u8 i)
{
	if(i==0)
		{
			OLED_WR_Byte(0xC8,OLED_CMD);//正常显示
			OLED_WR_Byte(0xA1,OLED_CMD);
		}
	if(i==1)
		{
			OLED_WR_Byte(0xC0,OLED_CMD);//反转显示
			OLED_WR_Byte(0xA0,OLED_CMD);
		}
}

void OLED_WR_Byte(u8 dat,u8 cmd)
{	
	u8 i;			  
	if(cmd)
		OLED_DC = 1;
	else 	
		OLED_DC = 0;
		OLED_CS = 0;
	for(i=0;i<8;i++)
	{			  
		OLED_D0 = 0;
		if(dat&0x80)
		   OLED_D1 = 1;
		else 
		  OLED_D1 = 0;
		OLED_D0 = 1;
		dat<<=1;   
	}				 		  
	OLED_CS = 1;
	OLED_DC = 1; 	  
}

//开启OLED显示 
void OLED_DisPlay_On(void)
{
	OLED_WR_Byte(0x8D,OLED_CMD);//电荷泵使能
	OLED_WR_Byte(0x14,OLED_CMD);//开启电荷泵
	OLED_WR_Byte(0xAF,OLED_CMD);//点亮屏幕
}

//关闭OLED显示 
void OLED_DisPlay_Off(void)
{
	OLED_WR_Byte(0x8D,OLED_CMD);//电荷泵使能
	OLED_WR_Byte(0x10,OLED_CMD);//关闭电荷泵
	OLED_WR_Byte(0xAE,OLED_CMD);//关闭屏幕
}

//清除更新记录
void Clear_Update(void)
{
	u8 i;
	for(i=0;i<128;i++) OLED_UPDATE[i] = 0x00;
}

//更新显存到OLED	
void OLED_Refresh(void)
{
	u8 i,n;
	for(i=0;i<8;i++)
	{
	   OLED_WR_Byte(0xb0+i,OLED_CMD); //设置页
	   OLED_WR_Byte(0x00,OLED_CMD);   //设置低列起始地址
	   OLED_WR_Byte(0x10,OLED_CMD);   //设置高列起始地址
	   for(n=0;n<128;n++)
		 OLED_WR_Byte(OLED_GRAM[n][i],OLED_DATA);
  }
}

void MyOLED_Refresh(void)
{
	u8 i,n;
		
	for(i=0;i<8;i++)
	{
		OLED_WR_Byte(0xb0+i,OLED_CMD); //设置页
		for(n=0;n<128;n++)
		{
			if( OLED_UPDATE[n] & 1<<i ) //找到需要更新的page
			{
				OLED_WR_Byte(0x00+(n&0x0F),OLED_CMD);   //设置低列起始地址
				OLED_WR_Byte(0x10+(n>>4),OLED_CMD);    //设置高列起始地址
				OLED_WR_Byte(OLED_GRAM[n][i],OLED_DATA);
			}
		}
	}
	Clear_Update();
}

//清屏函数
void OLED_Clear(void)
{
	u8 i,n;
	for(i=0;i<8;i++)
	{
	   for(n=0;n<128;n++)
		{
			OLED_GRAM[n][i]=0;//清除所有数据
			OLED_UPDATE[n] |= 1<<n;
		}
  }
	for(i=0;i<128;i++) OLED_UPDATE[i] = 0xFF;
//	MyOLED_Refresh();
}

//画点 
//x:0~127
//y:0~63
//t:1 填充 0,清空	
void OLED_DrawPoint(u8 x,u8 y,u8 t)
{
	if( x > 127 || y > 63 ) return;
	u8 i,m,n;
	i=y/8;    //得到page
	m=y%8;    //seg偏移
	n=1<<m;
	
	if( t ) //如果填充
	{
		if( ~(OLED_GRAM[x][i] & n) )
		{
			OLED_GRAM[x][i]|=n;
			OLED_UPDATE[x] |= 1<<i;
		}
			
			
	}
	else  //如果清空
	{
		if( OLED_GRAM[x][i] | n )
		{
			OLED_GRAM[x][i]=~OLED_GRAM[x][i];
			OLED_GRAM[x][i]|=n;
		    OLED_GRAM[x][i]=~OLED_GRAM[x][i];
			OLED_UPDATE[x] |= 1<<i;
		}		
	}
}

//画线
//x1,y1:起点坐标
//x2,y2:结束坐标
void OLED_DrawLine(u8 x1,u8 y1,u8 x2,u8 y2,u8 mode)
{
	u8 temp;
	if( x1 > x2 ) {temp = x1;x1=x2;x2=temp;}
	if( y1 > y2 ) {temp = y1;y1=y2;y2=temp;}
	u16 t; 
	int xerr=0,yerr=0,delta_x,delta_y,distance;
	int incx,incy,uRow,uCol;
	delta_x=x2-x1; //计算坐标增量 
	delta_y=y2-y1;
	uRow=x1;//画线起点坐标
	uCol=y1;
	if(delta_x>0)incx=1; //设置单步方向 
	else if (delta_x==0)incx=0;//垂直线 
	else {incx=-1;delta_x=-delta_x;}
	if(delta_y>0)incy=1;
	else if (delta_y==0)incy=0;//水平线 
	else {incy=-1;delta_y=-delta_x;}
	if(delta_x>delta_y)distance=delta_x; //选取基本增量坐标轴 
	else distance=delta_y;
	for(t=0;t<distance+1;t++)
	{
		OLED_DrawPoint(uRow,uCol,mode);//画点
		xerr+=delta_x;
		yerr+=delta_y;
		if(xerr>distance)
		{
			xerr-=distance;
			uRow+=incx;
		}
		if(yerr>distance)
		{
			yerr-=distance;
			uCol+=incy;
		}
	}
}


//画直角矩形函数
//x,y:左上角坐标
//dx,dy2:长，宽
//is_fill:是否填充
void OLED_DrawBoxR(u8 x,u8 y,u8 dx,u8 dy,u8 is_fill,u8 mode)
{
	u8 i,j;
	if(is_fill)
	{
		for(i=0;i<dx;i++)
		{
			for(j=0;j<dy;j++)
			{
				OLED_DrawPoint(x+i,y+j,mode);
			}
		}
	}
	else
	{
		for(i=0;i<dx;i++)
		{
			OLED_DrawPoint(x+i,y,mode);
			OLED_DrawPoint(x+i,y+dy-1,mode);
		}
		for(i=0;i<dy;i++)
		{
			OLED_DrawPoint(x,y+i,mode);
			OLED_DrawPoint(x+dx-1,y+i,mode);
		}

	}
}

//画直角矩形函数
//x,y:左上角坐标
//dx,dy2:长，宽
//r:圆角半径
//is_fill:是否填充
void OLED_DrawBoxC(u8 x,u8 y,u8 dx,u8 dy,u8 r,u8 is_fill,u8 mode)
{
	u8 i,j;
	if(is_fill)
	{
		for(i=0;i<dx-2*r;i++)
		{
			for(j=0;j<dy-2*r;j++)
			{
				OLED_DrawPoint(i,j,mode);
			}
		}
	}
	else
	{
		for(i=0;i<dx-2*r;i++)
		{
			OLED_DrawPoint(x+i+r,y,mode);
			OLED_DrawPoint(x+i+r,y+dy-1,mode);
		}
		for(i=0;i<dy-2*r;i++)
		{
			OLED_DrawPoint(x,y+i+r,mode);
			OLED_DrawPoint(x+dx-1,y+i+r,mode);
		}

	}

	quick_circle(x+dx-1-r,y+r,r,0x03,mode);
	quick_circle(x+dx-1-r,y+dy-1-r,r,0x0C,mode);
	quick_circle(x+r,y+dy-1-r,r,0x30,mode);
	quick_circle(x+r,y+r,r,0xC0,mode);
}


////x,y:圆心坐标
////r:圆的半径
//void OLED_DrawCircle(u8 x,u8 y,u8 r)
//{
//	int a, b,num;
//    a = 0;
//    b = r;
//    while(2 * b * b >= r * r)      
//    {
//        OLED_DrawPoint(x + a, y - b,1);
//        OLED_DrawPoint(x - a, y - b,1);
//        OLED_DrawPoint(x - a, y + b,1);
//        OLED_DrawPoint(x + a, y + b,1);
// 
//        OLED_DrawPoint(x + b, y + a,1);
//        OLED_DrawPoint(x + b, y - a,1);
//        OLED_DrawPoint(x - b, y - a,1);
//        OLED_DrawPoint(x - b, y + a,1);
//        
//        a++;
//        num = (a * a + b * b) - r*r;//计算画的点离圆心的距离
//        if(num > 0)
//        {
//            b--;
//            a--;
//        }
//    }
//}


//1/8圆变换
void draw_circle_8(u8 xc,u8 yc,u8 x,u8 y,u8 path,u8 mode)
{
	if( path & 1<<0 )
		OLED_DrawPoint(xc + x, yc - y,mode); //1
	if( path & 1<<1 )
		OLED_DrawPoint(xc + y, yc - x,mode); //2
	if( path & 1<<2 )
		OLED_DrawPoint(xc + y, yc + x,mode); //3
	if( path & 1<<3 )
		OLED_DrawPoint(xc + x, yc + y,mode); //4
	if( path & 1<<4 )
		OLED_DrawPoint(xc - x, yc + y,mode); //5
	if( path & 1<<5 )
		OLED_DrawPoint(xc - y, yc + x,mode); //6
	if( path & 1<<6 )
		OLED_DrawPoint(xc - y, yc - x,mode); //7
	if( path & 1<<7 )
		OLED_DrawPoint(xc - x, yc - y,mode); //8	
}


//快速画圆（1/8）
void quick_circle(u8 xc,u8 yc,u8 r,u8 path,u8 mode)
{
     int x, y, d;
     x = 0;
     y = r;
     d = -r / 2;
     draw_circle_8(xc , yc , x , y,path,mode);
     if(r % 2 == 0)
     {
         while(x < y)
         {
             x++;
             if(d < 0)
                 d += x;
             else
             {
                 y--;
                 d += x - y;
             }
             draw_circle_8(xc , yc , x , y,path,mode);
         }
     }
     else
     {
         while(x < y)
         {
             x++;
             if(d < 0)
               d += x + 1;
             else
             {
               y--;
               d += x - y + 1;
             }
             draw_circle_8(xc , yc , x , y,path,mode);
         }
     }
}

//画圆
void OLED_DrawCircle(u8 xc,u8 yc,u8 r,u8 mode)
{
	 quick_circle(xc,yc,r,0xFF,mode);
 }


//在指定位置显示一个字符,包括部分字符
//x:0~127
//y:0~63
//size1:选择字体 6x8/6x12/8x16/12x24
//mode:0,反色显示;1,正常显示
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size1,u8 mode)
{
	u8 i,m,temp,size2,chr1;
	u8 x0=x,y0=y;
	if(size1==8)size2=6;
	else size2=(size1/8+((size1%8)?1:0))*(size1/2);  //得到字体一个字符对应点阵集所占的字节数
	chr1=chr-' ';  //计算偏移后的值
	for(i=0;i<size2;i++)
	{
		if(size1==8)
			  {temp=ascll_0806[chr1][i];} //调用0806字体
		else if(size1==12)
        {temp=ascll_1206[chr1][i];} //调用1206字体
		else if(size1==16)
        {temp=ascll_1608[chr1][i];} //调用1608字体
		else if(size1==24)
        {temp=ascll_2412[chr1][i];} //调用2412字体
		else return;
		for(m=0;m<8;m++)
		{
			if(temp&0x01)OLED_DrawPoint(x,y,mode);
			else OLED_DrawPoint(x,y,!mode);
			temp>>=1;
			y++;
		}
		x++;
		if((size1!=8)&&((x-x0)==size1/2))
		{x=x0;y0=y0+8;}
		y=y0;
  }
}


//显示字符串
//x,y:起点坐标  
//size1:字体大小 
//*chr:字符串起始地址 
//mode:0,反色显示;1,正常显示
void OLED_ShowString(u8 x,u8 y,u8 *chr,u8 size1,u8 mode)
{
	while((*chr>=' ')&&(*chr<='~'))//判断是不是非法字符!
	{
		OLED_ShowChar(x,y,*chr,size1,mode);
		if(size1==8)x+=6;
		else x+=size1/2;
		chr++;
  }
}

//m^n
u32 OLED_Pow(u8 m,u8 n)
{
	u32 result=1;
	while(n--)
	{
	  result*=m;
	}
	return result;
}

//显示数字
//x,y :起点坐标
//num :要显示的数字
//len :数字的位数
//size:字体大小
//mode:0,反色显示;1,正常显示
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size1,u8 mode)
{
	u8 t,temp,m=0;
	if(size1==8)m=2;
	for(t=0;t<len;t++)
	{
		temp=(num/OLED_Pow(10,len-t-1))%10;
			if(temp==0)
			{
				OLED_ShowChar(x+(size1/2+m)*t,y,'0',size1,mode);
      }
			else 
			{
			  OLED_ShowChar(x+(size1/2+m)*t,y,temp+'0',size1,mode);
			}
  }
}

//显示汉字
//x,y:起点坐标
//num:汉字对应的序号
//mode:0,反色显示;1,正常显示
void OLED_ShowChinese(u8 x,u8 y,u8 num,u8 size1,u8 mode)
{
	u8 m,temp;
	u8 x0=x,y0=y;
	u16 i,size3=(size1/8+((size1%8)?1:0))*size1;  //得到字体一个字符对应点阵集所占的字节数
	for(i=0;i<size3;i++)
	{
		if(size1==16)
				{temp=Hzk1[num][i];}//调用16*16字体
		else if(size1==24)
				{temp=Hzk2[num][i];}//调用24*24字体
		else if(size1==32)       
				{temp=Hzk3[num][i];}//调用32*32字体
		else if(size1==64)
				{temp=Hzk4[num][i];}//调用64*64字体
		else return;
		for(m=0;m<8;m++)
		{
			if(temp&0x01)OLED_DrawPoint(x,y,mode);
			else OLED_DrawPoint(x,y,!mode);
			temp>>=1;
			y++;
		}
		x++;
		if((x-x0)==size1)
		{x=x0;y0=y0+8;}
		y=y0;
	}
}

//num 显示汉字的个数
//space 每一遍显示的间隔
//mode:0,反色显示;1,正常显示
void OLED_ScrollDisplay(u8 num,u8 space,u8 mode)
{
	u8 i,n,t=0,m=0,r;
	while(1)
	{
		if(m==0)
		{
	    OLED_ShowChinese(128,24,t,16,mode); //写入一个汉字保存在OLED_GRAM[][]数组中
			t++;
		}
		if(t==num)
			{
				for(r=0;r<16*space;r++)      //显示间隔
				 {
					for(i=1;i<144;i++)
						{
							for(n=0;n<8;n++)
							{
								OLED_GRAM[i-1][n]=OLED_GRAM[i][n];
							}
						}
           OLED_Refresh();
				 }
        t=0;
      }
		m++;
		if(m==16){m=0;}
		for(i=1;i<144;i++)   //实现左移
		{
			for(n=0;n<8;n++)
			{
				OLED_GRAM[i-1][n]=OLED_GRAM[i][n];
			}
		}
		OLED_Refresh();
	}
}

//x,y：起点坐标
//sizex,sizey,图片长宽
//BMP[]：要写入的图片数组
//mode:0,反色显示;1,正常显示
void OLED_ShowPicture(u8 x,u8 y,u8 sizex,u8 sizey,u8 BMP[],u8 mode)
{
	u16 j=0;
	u8 i,n,temp,m;
	u8 x0=x,y0=y;
	sizey=sizey/8+((sizey%8)?1:0);
	for(n=0;n<sizey;n++)
	{
		 for(i=0;i<sizex;i++)
		 {
				temp=BMP[j];
				j++;
				for(m=0;m<8;m++)
				{
					if(temp&0x01)OLED_DrawPoint(x,y,mode);
					else OLED_DrawPoint(x,y,!mode);
					temp>>=1;
					y++;
				}
				x++;
				if((x-x0)==sizex)
				{
					x=x0;
					y0=y0+8;
				}
				y=y0;
     }
	 }
}
//OLED的初始化
void OLED_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
 	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO , ENABLE );	 //使能A端口时钟
	
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE); //关闭SWJ功能
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOA, &GPIO_InitStructure);	  
 	GPIO_SetBits(GPIOA,GPIO_Pin_15);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6;
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOB, &GPIO_InitStructure);	  
 	GPIO_SetBits(GPIOB,GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6);

	OLED_RES = 0;
	delay_ms(200);
	OLED_RES = 1;
	
	OLED_WR_Byte(0xAE,OLED_CMD);//--turn off oled panel
	OLED_WR_Byte(0x00,OLED_CMD);//---set low column address
	OLED_WR_Byte(0x10,OLED_CMD);//---set high column address
	OLED_WR_Byte(0x40,OLED_CMD);//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
	OLED_WR_Byte(0x81,OLED_CMD);//--set contrast control register
	OLED_WR_Byte(0xC0,OLED_CMD);// Set SEG Output Current Brightness
	OLED_WR_Byte(0xA1,OLED_CMD);//--Set SEG/Column Mapping     0xa0左右反置 0xa1正常
	OLED_WR_Byte(0xC8,OLED_CMD);//Set COM/Row Scan Direction   0xc0上下反置 0xc8正常
	OLED_WR_Byte(0xA6,OLED_CMD);//--set normal display
	OLED_WR_Byte(0xA8,OLED_CMD);//--set multiplex ratio(1 to 64)
	OLED_WR_Byte(0x3f,OLED_CMD);//--1/64 duty
	OLED_WR_Byte(0xD3,OLED_CMD);//-set display offset	Shift Mapping RAM Counter (0x00~0x3F)
	OLED_WR_Byte(0x00,OLED_CMD);//-not offset
	OLED_WR_Byte(0xd5,OLED_CMD);//--set display clock divide ratio/oscillator frequency
	OLED_WR_Byte(0x80,OLED_CMD);//--set divide ratio, Set Clock as 100 Frames/Sec
	OLED_WR_Byte(0xD9,OLED_CMD);//--set pre-charge period
	OLED_WR_Byte(0xF1,OLED_CMD);//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
	OLED_WR_Byte(0xDA,OLED_CMD);//--set com pins hardware configuration
	OLED_WR_Byte(0x12,OLED_CMD);
	OLED_WR_Byte(0xDB,OLED_CMD);//--set vcomh
	OLED_WR_Byte(0x40,OLED_CMD);//Set VCOM Deselect Level
	OLED_WR_Byte(0x20,OLED_CMD);//-Set Page Addressing Mode (0x00/0x01/0x02)
	OLED_WR_Byte(0x02,OLED_CMD);//
	OLED_WR_Byte(0x8D,OLED_CMD);//--set Charge Pump enable/disable
	OLED_WR_Byte(0x14,OLED_CMD);//--set(0x10) disable
	OLED_WR_Byte(0xA4,OLED_CMD);// Disable Entire Display On (0xa4/0xa5)
	OLED_WR_Byte(0xA6,OLED_CMD);// Disable Inverse Display On (0xa6/a7) 
	
	OLED_Clear();
	MyOLED_Refresh();
	OLED_WR_Byte(0xAF,OLED_CMD);
}



/************************************************************************************************************************************/



//void MyPrint(u8 x,u8 y,u8 *tag,u8 dx,u8 dy,u8 mode,u8 is_cover)
//{
//	u32 i;
//	u8 j,x1,y1,temp;
//	u16 byte_num;
//	x1 = x; y1 = y;
//	if( dy % 8 ) dy = (dy/8+1)*8;
//	byte_num = dx*dy/8;
//	
//	for(i=0;i<byte_num;i++)
//	{
//		temp = tag[i];
//		for(j=0;j<8;j++)
//		{
//			if( temp&0x01 ) OLED_DrawPoint(x1,y1,mode);
//			else OLED_DrawPoint(x1,y1,(~mode)&0x01);
//			temp >>= 1;
//			y1++;
//		}
//		x1++;
//		if( x1 - x == dx) x1 = x;
//		else y1-=8;
//	}	
//}

