#ifndef __OLED_H
#define __OLED_H 

#include "sys.h"
#include "stdlib.h"	
//#include "oledfont.h"
//-----------------OLED端口定义---------------- 

#define OLED_D0   PAout(15)
#define OLED_D1   PBout(3)
#define OLED_RES  PBout(4)
#define OLED_DC   PBout(5)
#define OLED_CS   PBout(6)



#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

void MyOLED_Refresh(void);
//void MyPrint(u8 x,u8 y,u8 *tag,u8 dx,u8 dy,u8 mode,u8 is_cover);
void OLED_ClearPoint(u8 x,u8 y);
void OLED_ColorTurn(u8 i);
void OLED_DisplayTurn(u8 i);
void OLED_WR_Byte(u8 dat,u8 mode);
void OLED_DisPlay_On(void);
void OLED_DisPlay_Off(void);
void OLED_Refresh(void);
void OLED_Clear(void);
void OLED_DrawPoint(u8 x,u8 y,u8 t);
void OLED_DrawLine(u8 x1,u8 y1,u8 x2,u8 y2,u8 mode);
//void OLED_DrawCircle(u8 x,u8 y,u8 r);
void OLED_DrawCircle(u8 xc,u8 yc,u8 r,u8 mode);
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size1,u8 mode);
void OLED_ShowChar6x8(u8 x,u8 y,u8 chr,u8 mode);
void OLED_ShowString(u8 x,u8 y,u8 *chr,u8 size1,u8 mode);
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size1,u8 mode);
void OLED_ShowChinese(u8 x,u8 y,u8 num,u8 size1,u8 mode);
void OLED_ScrollDisplay(u8 num,u8 space,u8 mode);
void OLED_ShowPicture(u8 x,u8 y,u8 sizex,u8 sizey,u8 BMP[],u8 mode);
void OLED_Init(void);

void draw_circle_8(u8 xc,u8 yc,u8 x,u8 y,u8 path,u8 mode);
void quick_circle(u8 xc,u8 yc,u8 r,u8 path,u8 mode);
void OLED_DrawBoxR(u8 x,u8 y,u8 dx,u8 dy,u8 is_fill,u8 mode);
void OLED_DrawBoxC(u8 x,u8 y,u8 dx,u8 dy,u8 r,u8 is_fill,u8 mode);

#endif

