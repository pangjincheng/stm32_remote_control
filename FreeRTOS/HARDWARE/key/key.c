/*
PA5  switch_left
PA6  switch_right
PA12 switch_l
PB8  switch_up
PB9  switch_down
PB11 swtich_r
*/

#include "key.h"
#include "sys.h"
#include "delay.h"
#include "usart.h" //text

u8 Key_Status = 0;

void key_init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO ,ENABLE);


	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_12;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_11;
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	

	EXTI_InitTypeDef EXTI_InitStruct;	
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStruct.EXTI_Line = EXTI_Line5;
	EXTI_Init(&EXTI_InitStruct);
	EXTI_InitStruct.EXTI_Line = EXTI_Line6;
	EXTI_Init(&EXTI_InitStruct);
	EXTI_InitStruct.EXTI_Line = EXTI_Line8;
	EXTI_Init(&EXTI_InitStruct);
	EXTI_InitStruct.EXTI_Line = EXTI_Line9;
	EXTI_Init(&EXTI_InitStruct);
	EXTI_InitStruct.EXTI_Line = EXTI_Line11;
	EXTI_Init(&EXTI_InitStruct);
	EXTI_InitStruct.EXTI_Line = EXTI_Line12;
	EXTI_Init(&EXTI_InitStruct);

	EXTI_ClearITPendingBit(EXTI_Line5);	
	EXTI_ClearITPendingBit(EXTI_Line6);	
	EXTI_ClearITPendingBit(EXTI_Line8);	
	EXTI_ClearITPendingBit(EXTI_Line9);	
	EXTI_ClearITPendingBit(EXTI_Line11);	
	EXTI_ClearITPendingBit(EXTI_Line12);	



	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource5);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource6);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource12);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource8);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource9);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource11);
	
}


u8 key_scan(void)
{
	if( ( KEY_UP | KEY_DOWN | KEY_LEFT | KEY_RIGHT | KEY_L | KEY_R ) )
	{
		delay_ms(20);
		if     ( KEY_UP )    return KEY_UP_PRES;
		else if( KEY_DOWN )  return KEY_DOWN_PRES;
		else if( KEY_LEFT )  return KEY_LEFT_PRES;
		else if( KEY_RIGHT ) return KEY_RIGHT_PRES;
		else if( KEY_L )     return KEY_L_PRES;
		else if( KEY_R )     return KEY_R_PRES;
	}
	return 0;
}


/*******************************************************************************************************************/
/*                                                   �жϺ���                                                      */
/*******************************************************************************************************************/

/*
PA5  switch_left
PA6  switch_right
PA12 switch_l
PB8  switch_up
PB9  switch_down
PB11 swtich_r
*/

//void EXTI9_5_IRQHandler(void)
//{
//	if( EXTI_GetITStatus(EXTI_Line5) == SET ) //switch_left
//	{
////		printf("PA5\r\n");
//		Key_Status |= KEY_LEFT;
//		EXTI_ClearITPendingBit(EXTI_Line5);

//	}
//	if( EXTI_GetITStatus(EXTI_Line6) == SET ) //switch_right
//	{
//		printf("PA6\r\n");
//		Key_Status |= KEY_RIGHT;
//		EXTI_ClearITPendingBit(EXTI_Line6);
//		
//	}
//	if( EXTI_GetITStatus(EXTI_Line8) == SET ) //switch_up
//	{
//		if(PBin(8)) printf("PB8\r\n");
//		Key_Status |= KEY_UP;
//		EXTI_ClearITPendingBit(EXTI_Line8);
//	}
//	if( EXTI_GetITStatus(EXTI_Line9) == SET ) //switch_down
//	{
//		if(PBin(9))printf("PB9\r\n");
//		Key_Status |= KEY_DOWN;
//		EXTI_ClearITPendingBit(EXTI_Line9);
//	}
//}

//void EXTI15_10_IRQHandler(void)
//{
//	if( EXTI_GetITStatus(EXTI_Line12) == SET ) //switch_l
//	{
//		printf("PA12\r\n");
//		Key_Status |= KEY_L;
//		EXTI_ClearITPendingBit(EXTI_Line12);

//	}
//	if( EXTI_GetITStatus(EXTI_Line11) == SET ) //switch_r
//	{
//		printf("PB11\r\n");
//		Key_Status |= KEY_R;
//		EXTI_ClearITPendingBit(EXTI_Line11);
//		
//	}
//}
