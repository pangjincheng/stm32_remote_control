#ifndef __SWITCH_H
#define __SWITCH_H
#include "sys.h"

#define KEY_UP     PBin(8)
#define KEY_DOWN   PBin(9)
#define KEY_LEFT   PAin(5)
#define KEY_RIGHT  PAin(6)
#define KEY_L      PAin(12)
#define KEY_R      PBin(11)

#define KEY_UP_PRES     (1<<0)
#define KEY_DOWN_PRES   (1<<1)
#define KEY_LEFT_PRES   (1<<2)
#define KEY_RIGHT_PRES  (1<<3)
#define KEY_L_PRES      (1<<4)
#define KEY_R_PRES      (1<<5)

void key_init(void);
u8 key_scan(void);
  






#endif
