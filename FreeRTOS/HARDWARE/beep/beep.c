/*
PA0 TIM2_CH1
*/

#include "beep.h"
#include "sys.h"


void beep_init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStruct;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_OCInitTypeDef TIM_OCInitStruct;

	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	TIM_TimeBaseInitStruct.TIM_Prescaler=3-1;     //PSC
	TIM_TimeBaseInitStruct.TIM_Period=10000-1;    //ARR
	TIM_TimeBaseInitStruct.TIM_ClockDivision=TIM_CKD_DIV1; // 不分频
	TIM_TimeBaseInitStruct.TIM_CounterMode=TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter=0; //重复计数
	TIM_TimeBaseInit(TIM2,&TIM_TimeBaseInitStruct);
	
	TIM_OCInitStruct.TIM_OCMode=TIM_OCMode_PWM1; //PW1模式
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High; //有效电平为高
	TIM_OCInitStruct.TIM_OutputState=TIM_OutputState_Enable; //使能输出
	TIM_OCInitStruct.TIM_Pulse=0; //占空比。 0 - 36000 , 由全灭到全亮。（15000即可过电调的pwm检测）
	TIM_OC2Init(TIM2,&TIM_OCInitStruct); //设置为TIM2的CH2

	TIM_OC2PreloadConfig(TIM2,ENABLE); //CCR更改后会在下一个比较周期更新
	
	TIM_Cmd(TIM2,ENABLE); //使能TIM2
	
	TIM_CtrlPWMOutputs(TIM4,ENABLE); //使能PWM输出？不能少！

}

//设置发声频率
void beep_set(u16 value)
{
	TIM_PrescalerConfig(TIM2,value,TIM_PSCReloadMode_Immediate); //修改PSC的值
}

void beep_on(void)
{
	TIM_SetCompare1(TIM2,5000);
}

void beep_off(void)
{
	TIM_SetCompare1(TIM2,0);
}

//	GPIO_InitTypeDef GPIO_InitStruct;
//	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
//	TIM_OCInitTypeDef TIM_OCInitStruct;
//	
//	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7;
//	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
//	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOB,&GPIO_InitStruct);
//	
//	TIM_TimeBaseInitStruct.TIM_Prescaler=3-1;     //PSC
//	TIM_TimeBaseInitStruct.TIM_Period=10000-1;    //ARR
//	TIM_TimeBaseInitStruct.TIM_ClockDivision=TIM_CKD_DIV1; // 不分频
//	TIM_TimeBaseInitStruct.TIM_CounterMode=TIM_CounterMode_Up;
//	TIM_TimeBaseInitStruct.TIM_RepetitionCounter=0; //重复计数
//	TIM_TimeBaseInit(TIM2,&TIM_TimeBaseInitStruct);
//	
//	TIM_OCInitStruct.TIM_OCMode=TIM_OCMode_PWM1; //PW1模式
//	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High; //有效电平为高
//	TIM_OCInitStruct.TIM_OutputState=TIM_OutputState_Enable; //使能输出
//	TIM_OCInitStruct.TIM_Pulse=0; //占空比。 0 - 36000 , 由全灭到全亮。（15000即可过电调的pwm检测）
//	TIM_OC2Init(TIM4,&TIM_OCInitStruct); //设置为TIM4的CH2

//	TIM_OC2PreloadConfig(TIM4,ENABLE); //CCR更改后会在下一个比较周期更新
//	
//	TIM_Cmd(TIM4,ENABLE); //使能TIM2
//	
//	TIM_CtrlPWMOutputs(TIM4,ENABLE); //使能PWM输出？不能少！
//}

