#include "mymath.h"

//快速排列测试代码
void quicksort(u32 left,u32 right,u32* array)
{
	u32 i,j,t,temp;
	if( (left >= right) || (right == 0xFFFFFFFF) ) return;
	
	temp = array[left]; //基准数
	i = left;
	j = right;
	while(i != j)
	{
		//先从右往左找
		while( (array[j] >= temp) && (i < j) ) j--;
		//再从左往右找
		while( (array[i] <= temp) && (i < j) ) i++;
		
		//交换
		if( i < j )
		{
			      t  = array[i];
			array[i] = array[j];
			array[j] = t;
		}
	}
	
	//将基准数归位
	array[left] = array[i];
	   array[i] = temp;
	
	if( left != i )  quicksort(left,i-1,array);  //继续递归处理左边的数
	if( i+1 != right ) quicksort(i+1,right,array); //继续递归处理右边的数
}
