/*
以下函数使用前需先调用  RTC_WaitForLastTask();
RTC_ITConfig
RTC_SetCounter
RTC_SetPrescaler
RTC_SetAlarm
RTC_ClearFlag
RTC_ClearITPendingBit
*/

#include "rtc.h"
#include "stm32f10x_pwr.h"
#include "mymath.h"
#include "usart.h"

ALARM RTC_Alarm[RTC_ALARM_NUM];
vu8 mark_alarm_using = 0;
vu8 mark_alarm_num = 0;

//月份数据表
const u8 table_week[12] = {0,3,3,6,1,4,6,2,5,0,3,5}; //月修正数据表
//平年月份日期表
const u8 mon_table[12]={31,28,31,30,31,30,31,31,30,31,30,31};






/********************************************************************************************************************************/
/*                                                    对外提供的函数                                                            */
/********************************************************************************************************************************/

void RTC_Init(void)
{
	//使能电源时钟和备份区域时钟
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_PWR | RCC_APB1Periph_BKP , ENABLE );
	//硬复位后，需取消备份区域的写保护
	PWR_BackupAccessCmd(ENABLE);
	
	BKP_DeInit(); //复位
	RCC_LSEConfig(RCC_LSE_ON); //使用外部低速晶振
	while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET); //等待低速晶振就绪
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE); //使用LSE作为RTC时钟
	RCC_RTCCLKCmd(ENABLE); //使能RTC时钟
	RTC_WaitForSynchro(); //等待RTC寄存器同步
	RTC_WaitForLastTask(); //等待RTC寄存器操作完成
	RTC_ITConfig(RTC_IT_SEC | RTC_IT_ALR,ENABLE); //使能RTC秒中断
	RTC_EnterConfigMode(); //允许配置
	RTC_WaitForLastTask(); //等待RTC寄存器操作完成
	RTC_SetPrescaler(32768-1); //RTC预分频
	RTC_Set_Date(2020,7,8,15,55,00); //设置默认当前时间
	RTC_ExitConfigMode(); //退出配置模式
	
	RTC_NVIC_Config();
}


u8 RTC_Set_Date(u16 _year,u8 _mon,u8 _day,u8 _hour,u8 _min,u8 _sec)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP , ENABLE); //使能PWR和BKP外设时钟  
	PWR_BackupAccessCmd(ENABLE); //使能RTC和后背寄存器访问
	RTC_WaitForLastTask(); //等待RTC寄存器操作完成
	RTC_SetCounter( RTC_DateToSec(_year,_mon,_day,_hour,_min,_sec) );   
	return 0;	
}


u8 RTC_Add_Alarm(u16 _year,u8 _mon,u8 _day,u8 _hour,u8 _min,u8 _sec)
{
	RTC_WaitForLastTask();
	
	if( RTC_ALARM_NUM == mark_alarm_num ) 
	{
		printf("full\r\n");
		return 1; //满
	}
	
	u32 now = RTC_GetCounter();
	if( RTC_DateToSec(_year,_mon,_day,_hour,_min,_sec) <= now )
	{
		printf("history\r\n");
		return 2; //过期
	}
	
	u8 i;
	for(i=0;i<RTC_ALARM_NUM;i++)
	{
		if( RTC_Alarm[i].is_use == 0 ) continue; //忽略无效数据
		
		if( \
		RTC_Alarm[i].date.year == _year \
		&& RTC_Alarm[i].date.mon == _mon \
		&& RTC_Alarm[i].date.day == _day \
		&& RTC_Alarm[i].date.hour == _hour \
		&& RTC_Alarm[i].date.min == _min \
		&& RTC_Alarm[i].date.sec == _sec  )
		{
			printf("repeat\r\n");
			return 3; //重复设置
		}



	}
	
	i=0;
	while( RTC_Alarm[i].is_use == 1 ) i++;
	
	RTC_Alarm[i].date.year = _year;
	RTC_Alarm[i].date.mon = _mon;
	RTC_Alarm[i].date.day = _day;
	RTC_Alarm[i].date.hour = _hour;
	RTC_Alarm[i].date.min = _min;
	RTC_Alarm[i].date.sec = _sec;
	RTC_Alarm[i].is_repeat = 0;
	RTC_Alarm[i].is_use = 1;
	
	mark_alarm_num++;
	printf("add alarm : %d.%d.%d %d:%d:%d success\r\n",_year,_mon,_day,_hour,_min,_sec);
	
	//使系统闹钟为最近即将到来的时间
	RTC_Alarm_Update();
	return 0;
}






/********************************************************************************************************************************/
/*                                                   不对外提供的函数                                                           */
/********************************************************************************************************************************/

void RTC_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = RTC_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
	NVIC_Init(&NVIC_InitStruct);
}


u8 Is_Leap_Year(u16 _year)
{
	if( _year % 4 == 0 ) 
	{
		if( _year % 100 == 0 )
		{
			if( _year % 400 == 0 ) return 1;
			else return 0;
		}
		else return 1;
	}
	else return 0;
}


u8 RTC_Get_Week(u16 _year,u8 _mon,u8 _day)
{
	u16 temp;
	u8 yearH,yearL;
	
	yearH = _year / 100; //世纪
	yearL = _year % 100; //年份
	
	if( yearH > 19 ) yearL += 100; //如果为21世纪,年份数加100
	
	temp = yearL + yearL / 4;
	temp = temp % 7;
	temp = temp + _day + table_week[_mon-1];
	
	if( yearL % 4 == 0 && _mon < 3 ) temp --;
	return ( temp % 7 );
}


void RTC_SecToDate(u32 _sec,DATE *_p_date)
{
	u32 temp = 0;
	u16 temp1 = 0;	
	temp = _sec/86400; //得到天数		
	temp1 = 1970;
	
	//计算年份
	while( temp >= 365 )
	{
		if(Is_Leap_Year(temp1) ) //如果是闰年
		{
			if( temp >=366 )
			{
				temp -= 366;
				temp1++;
			}
		}
		else
		{
			temp -= 365;
			temp1++;
		}
	}
	_p_date->year = temp1;
	
	//计算月份和日期
	temp1 = 0;
 if( temp >= mon_table[temp1] )                                                                                                                                                                                       	while( temp >= mon_table[temp1] )
	{
		if( Is_Leap_Year(_p_date->year) && temp1 == 1 ) //闰年月处理
		{
			if( temp >= 29 )
			{
				temp -= 29;
				temp1++;
			}
			else break;
		}
		else
		{
			temp -= mon_table[temp1];
			temp1++;
		}
	}
	_p_date->mon = temp1 + 1;
	_p_date->day = temp + 1;

	temp = _sec % 86400; //得到一天中的秒数
	_p_date->hour = temp / 3600;
	
	_p_date->min = (temp % 3600) / 60;
	_p_date->sec = (temp % 3600) % 60;
	
	_p_date->msec = ((32767-RTC_GetDivider())*1000/32767);
	_p_date->week = RTC_Get_Week(_p_date->year,_p_date->mon,_p_date->day);	
}


u32 RTC_DateToSec(u16 _year,u8 _mon,u8 _day,u8 _hour,u8 _min,u8 _sec)
{
		u16 t;
	u32 seccount = 0;
	
	if( _year < 1970 || _year > 2099 ) return 1; //年份超出范围

	//将年转换为秒
	for(t=1970;t<_year;t++) //将所有年份的秒相加
	{
		if( Is_Leap_Year(t) ) seccount += 31622400; //闰年秒数
		else seccount+=31536000; //平年秒数
	}
	
	//将当前年份的各个月转换成秒
	for(t=0;t<_mon-1;t++)
	{
		seccount += (u32)mon_table[t]*86400; //一天有86400秒
		if( Is_Leap_Year(_year) && t == 1 ) seccount += 86400; //闰年2月份加一天的秒数
	}
	
	//将当前的日转换成秒
	seccount += (u32)(_day-1)*86400;
	
	//将当前的时转换成秒
	seccount += (u32)_hour*3600;

	//将当前的分转换成秒
	seccount += (u32)_min*60;
	
	//加上当前的秒
	seccount += _sec;
	
	return seccount;
}


u8 RTC_Set_Alarm(u32 _sec)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP,ENABLE); //使能PWR和BKP外设时钟
	PWR_BackupAccessCmd(ENABLE); //使能后背寄存器访问
	
	RTC_WaitForLastTask(); //等待RTC寄存器操作完成
	RTC_SetAlarm(_sec);
//	printf("RTC_Set_Alarm : %d\r\n",_sec);
	return 0;	
}


//使系统闹钟设定为表中离现在最近的时间
u8 RTC_Alarm_Update(void)
{
	if ( mark_alarm_num == 0 ) return 1; //表中无闹钟
	
	u8 i,first_num = 0;
	u32 temp,first_time = 0xFFFFFFFF;
	
	for(i=0;i<RTC_ALARM_NUM;i++)
	{
		if( RTC_Alarm[i].is_use == 0 ) continue; //忽略无效数据
		
		temp = RTC_DateToSec(RTC_Alarm[i].date.year,RTC_Alarm[i].date.mon,RTC_Alarm[i].date.day,RTC_Alarm[i].date.hour,RTC_Alarm[i].date.min,RTC_Alarm[i].date.sec);
		if ( temp < first_time )
		{
			first_time = temp;
			first_num = i;
		}
	}
	mark_alarm_using = first_num;
	RTC_Set_Alarm(first_time);
	printf("first_num : %d\r\n",first_num);
	printf("first_time : %d\r\n",first_time);

	return 0;
}


u32 RTC_Get_Date(DATE *_p_date)
{
	u32 temp = RTC_GetCounter();
	RTC_SecToDate(temp,_p_date);
	return temp;	
}


void RTC_Print_Date(void)
{
	DATE temp;
	RTC_Get_Date(&temp);
	printf("%d.%d.%d %d:%d:%d %d\r\n",temp.year,temp.mon,temp.day,temp.hour,temp.min,temp.sec,temp.week);
}



/********************************************************************************************************************************/
/*                                                     中断服务函数                                                             */
/********************************************************************************************************************************/

void RTC_IRQHandler(void)
{
	//秒中断
	if( RTC_GetITStatus(RTC_IT_SEC) == SET )
	{
//		RTC_Print_Date();
	}
	//闹钟中断
	if( RTC_GetITStatus(RTC_IT_ALR) == SET )
	{
		//打印
		printf("IT Alarm :");
		printf("%d.%d.%d %d:%d:%d %d\r\n",\
		RTC_Alarm[mark_alarm_using].date.year,\
		RTC_Alarm[mark_alarm_using].date.mon,\
		RTC_Alarm[mark_alarm_using].date.day,\
		RTC_Alarm[mark_alarm_using].date.hour,\
		RTC_Alarm[mark_alarm_using].date.min,\
		RTC_Alarm[mark_alarm_using].date.sec,\
		RTC_Alarm[mark_alarm_using].date.week);
		
		mark_alarm_num--;
		RTC_Alarm[mark_alarm_using].is_use = 0;
		RTC_Alarm_Update();
	}
	
	RTC_WaitForLastTask();
	RTC_ClearITPendingBit(RTC_IT_ALR | RTC_IT_SEC | RTC_IT_OW);
	
}


