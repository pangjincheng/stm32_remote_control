#ifndef RTC_H
#define RTC_H
#include "stm32f10x.h"
#include "usart.h"

#define RTC_ALARM_NUM 10

typedef struct
{
	vu16 year;  
	vu8 mon;    
	vu8 day;   
	vu8 hour;   
	vu8 min;    
	vu8 sec;    
	vu16 msec;  
	vu8 week;    //星期天为0
}DATE;

typedef struct
{
	DATE date;
	vu8 is_use;
	vu8 is_repeat;
	vu8 repeat_time;
}ALARM;


extern ALARM RTC_Alarm[RTC_ALARM_NUM];

/********************************************************************************************************************************/
/*                                                    对外提供的函数                                                            */
/********************************************************************************************************************************/

void RTC_Init(void);
u8 RTC_Set_Date(u16 _year,u8 _mon,u8 _day,u8 _hour,u8 _min,u8 _sec);
u8 RTC_Add_Alarm(u16 _year,u8 _mon,u8 _day,u8 _hour,u8 _min,u8 _sec);
void RTC_Print_Date(void);

/********************************************************************************************************************************/
/*                                                   不对外提供的函数                                                           */
/********************************************************************************************************************************/


void RTC_NVIC_Config(void);
u8 Is_Leap_Year(u16 _year);
u8 RTC_Get_Week(u16 _year,u8 _mon,u8 _day);
void RTC_SecToDate(u32 _sec,DATE *_p_date);
u32 RTC_DateToSec(u16 _year,u8 _mon,u8 _day,u8 _hour,u8 _min,u8 _sec);
u8 RTC_Set_Alarm(u32 _sec);
u8 RTC_Alarm_Update(void);
u32 RTC_Get_Date(DATE *_p_date);








#endif
