/*

SPI1_NSS  - PA4  //spi片选，未使用
SPI1_SCK  - PA5
SPI1_MISO - PA6
SPI1_MOSI - PA7

*/



#include "spi.h"

#define SPI_Wait_Timeout   ((u16)0xFFFF)



void MySPI_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_SPI1,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStruct;
	SPI_InitTypeDef SPI_InitStruct;
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA,&GPIO_InitStruct);

		
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStruct.SPI_CRCPolynomial = 7;
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
	SPI_InitStruct.SPI_NSS= SPI_NSS_Soft;
	
	SPI_Init(SPI1,&SPI_InitStruct);
	
	SPI_Cmd(SPI1,ENABLE);

}


//读写一个字节
u8 SPI_RW_Byte(u8 TxByte)
{
	u8 data;
	u16 time = 0;
	while(SPI_I2S_GetFlagStatus(SPI1,SPI_I2S_FLAG_TXE) == RESET) //等待之前的发送完成
	{
		if(SPI_Wait_Timeout == ++time)return 0;
	}
	
	//SPI1->DR = TxByte; //发送数据
	SPI_I2S_SendData(SPI1,TxByte);
	
	time = 0;
	
	while(SPI_I2S_GetFlagStatus(SPI1,SPI_I2S_FLAG_RXNE) == RESET) //等待接收完数据
	{
		if(SPI_Wait_Timeout == ++time) return 0;
	}
	
	//data = (u8)SPI1->DR; //取出收到的数据
	data = SPI_I2S_ReceiveData(SPI1);
	
	return data;
}


//读写多个字节
void SPI_RW_BUFF(u8 *ReadBuff,u8 *WriteBuff,u16 len)
{
	while(len--)
	{
		*ReadBuff = SPI_RW_Byte(*WriteBuff);  //交换数据、
		ReadBuff++;
		WriteBuff++;
	}
}
