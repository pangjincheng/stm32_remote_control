#ifndef _SPI_H
#define _SPI_H
#include "sys.h"

void MySPI_Init(void);
u8 SPI_RW_Byte(u8 TxByte);
void SPI_RW_BUFF(u8 *ReadBuff,u8 *WriteBuff,u16 len);


#endif
