#ifndef __IIC_H
#define __IIC_H

#include "sys.h"




static void iic_sda_in(void);

static void iic_sda_out(void);

void iic_delay(void);

void iic_init(void);

void iic_start(void);

void iic_stop(void);

u8 iic_wait(void);

void iic_ask(void);

void iic_nask(void);

void iic_send_byte(u8 _data);

u8 iic_read_byte(u8  _iic_ask);


#endif
