#include "iic.h"
#include "delay.h"
#include "sys.h"

//SCL
#define SCL_CLK  RCC_APB2Periph_GPIOB
#define SCL_PORT GPIOB
#define SCL_PIN  GPIO_Pin_8
//SDA
#define SDA_CLK  RCC_APB2Periph_GPIOB
#define SDA_PORT GPIOB
#define SDA_PIN GPIO_Pin_9


#define SDA_IN  iic_sda_in()
#define SDA_OUT iic_sda_out()

#define SCL    PBout(8) 		    //设置SCL输出电平
#define SDA    PBout(9) 		    //设置SDA输出电平	 
#define READ_SDA   PBin(9) 		    //检测SDA输入电平


static void iic_sda_in(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = SDA_PIN;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(SDA_PORT,&GPIO_InitStruct);
}

static void iic_sda_out(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = SDA_PIN;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SDA_PORT,&GPIO_InitStruct);
}






void iic_delay(void)
{
	delay_us(2);
}

void iic_init(void)
{					     
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd( SCL_CLK | SDA_CLK , ENABLE );//使能外设SCL、SDA时钟 
		
	GPIO_InitStructure.GPIO_Pin = SCL_PIN;	 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		
	GPIO_Init(SCL_PORT, &GPIO_InitStructure);				
	
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;	 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		
	GPIO_Init(SDA_PORT, &GPIO_InitStructure);				

	GPIO_SetBits(SCL_PORT,SCL_PIN);						 //SCL 输出高	
	GPIO_SetBits(SDA_PORT,SDA_PIN);						 //SCL 输出高	
}

void iic_start(void)
{
	/*
	SDA:    1 ***** 1->0
	SCL:    1 ***************** 1->0
	*/
	SDA_OUT;
	SCL = 1;
	SDA = 1;
	iic_delay();
	
	SDA = 0;
	iic_delay();
	SCL = 0;	
	
}

void iic_stop(void)
{
	/*
	SDA:    0 ***** 0->1
	SCL:    0 ***** 0->1
	*/
	SDA_OUT;
	SCL = 0;
	SDA = 0;
	iic_delay();
	SCL = 1;
	SDA = 1;
	iic_delay();
}


u8 iic_wait(void)
{
	u8 time;
	SDA_IN;
	SDA = 1;
	iic_delay();
	SCL = 1;
	iic_delay();
	while(READ_SDA == 1)
	{
		time++;
		if( time > 250 ) 
		{
			iic_stop();
			SDA_OUT;
			return 1;
		}
	}
	SCL = 0;
	return 0;
}

void iic_ask(void)
{
	SCL = 0;
	SDA_OUT;
	SDA = 0;
	iic_delay();
	SCL = 1;
	iic_delay();
	SCL = 0;
}

void iic_nask(void)
{
	SCL = 0;
	SDA_OUT;
	SDA = 1;
	iic_delay();
	SCL = 1;
	iic_delay();
	SCL = 0;
}

void iic_send_byte(u8 _data)
{
	/*
	SDA:   *** 0/1 **********************
	SCL:   0 ******* 0->1***** 1->0 *****
	*/
	SDA_OUT;
	u8 i;
	SCL = 0;
	for(i=0;i<8;i++)
	{
		SDA = (_data & 0x80) >> 7;
		_data <<= 1;
		SCL = 1;
		iic_delay();
		SCL = 0;
		iic_delay();
	}
}

u8 iic_read_byte(u8  _iic_ask)
{
	u8 i,temp=0;
	SDA_IN;
	for(i=0;i<8;i++)
	{
		SCL = 0;
		iic_delay();
		SCL = 1;
		temp <<= 1;
		if(READ_SDA) temp++;
		iic_delay();
	}
	
	if( _iic_ask ) iic_ask();
	else iic_nask();
	
	SDA_OUT;
	return temp;
}


