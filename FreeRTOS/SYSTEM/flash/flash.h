#ifndef _FLASH_H
#define _FLASH_H
#include "sys.h"
//#include "stm32f10x.h"

/*********************** 移植 *************************/
#define FLASH_SIZE 512          //FLASH容量（KByte）
#define FLSAH_EN_WRITE 1        //1使能写入，0失能写入
/******************************************************/

#define FLSAH_BASH 0x08000000

//判断页大小
#if FLASH_SIZE < 256
#define SECTOR_SIZE 1024
#else
#define SECTOR_SIZE 2048
#endif

//提供给用户的函数
void FLASH_Write(u32 Addr,u16 *pBuff,u16 len);

u16 FLASH_ReadHalfWord(u32 faddr);                        //读取一个半字（16位）

void FLASH_Read(u32 Addr,u16 *pBuff,u16 len);             //读出一段数据（16位）

void FLASH_Writ_NoCheck(u32 Addr,u16 *pBuff,u16 len);     //不检查的写入




#endif
