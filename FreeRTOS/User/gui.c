#include "gui.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "oled.h"
#include "key.h"
#include "delay.h"
#include "oledfont.h"

extern u16 ADC_Buffer[4];
//u16 ADC_Datum[4] = {2048,2048,2048,2048}; //基准


//typedef struct _GUI_LIST_TYPE
//{
//	u8 *list_title; //标题
//	u8 list_title_offset; //标题偏移（开始位置）
//	u8 list_num;   //数目
//	u8 *list_option; //名称数组指针
//	u8 list_option_len; //名称数组固定长度
//	u8 list_option_tag;   //用于显示的第一个名字
//}GUI_LIST_TYPE;



void guiListShowTitle(GUI_LIST_TYPE *list) //显示标题
{
	OLED_Clear();
	OLED_ShowString((list->list_title_offset),1,(list->list_title),12,1);
	OLED_DrawBoxC(0,0,128,16,5,0,1);	
}

void guiListShowNow(GUI_LIST_TYPE *list) //显示现在
{
	guiListShowTitle(list);
	u8 i;
	u8 tag = list->list_option_tag;
	u8 num = list->list_num;
	if( list->list_num < 3 )
	{
		for(i=0;i<list->list_num;i++)
		{
			OLED_ShowString(0,16*(i+1),"            ",16,1);
			OLED_ShowString(0,16*(i+1),list->list_option+list->list_option_len*tag,16,i == 1? 0:1);
			tag++;
			tag %= num;
		}	
	}
	else
	{
		for(i=0;i<3;i++)
		{
			OLED_ShowString(0,16*(i+1),"            ",16,1);
			OLED_ShowString(0,16*(i+1),list->list_option+list->list_option_len*tag,16,i == 1? 0:1);
			tag++;
			tag %= num;
		}
	}
	OLED_Refresh();	
}

void guiListShowNext(GUI_LIST_TYPE *list) //显示下一个
{
	list->list_option_tag = (list->list_option_tag+1)%list->list_num;
	guiListShowNow(list);
}

void guiListShowPrevious(GUI_LIST_TYPE *list) //显示上一个
{
	list->list_option_tag = (list->list_option_tag+list->list_num-1)%list->list_num;
	guiListShowNow(list);
}



//主菜单
void guiMenu(void)
{
	
	u8 gui_menu_name[][12] = {"Dashboard","PID","Setting",};
	void (*function[5])(void) = {guiDashboard,guiPIDMenu,guiSetting}; //用于存放函数指针

	GUI_LIST_TYPE list;
	list.list_num = 3;
	list.list_option = gui_menu_name[0];
	list.list_option_len = 12;
	list.list_option_tag = 0;
	list.list_title = "menu";
	list.list_title_offset = 54;
	
	guiListShowPrevious(&list);
	
	u8 key_value;
	while(1)
	{
		guiListShowNow(&list);
		
		key_value = guiInput();
		if( KEY_UP_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowPrevious(&list);
		}
		else if( KEY_DOWN_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowNext(&list);
		}
		else if( KEY_LEFT_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowPrevious(&list);
		}
		else if( KEY_RIGHT_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowNext(&list);
		}
		else if( KEY_R_PRES == key_value ) //退出
		{
			//无
		}
		else if( KEY_L_PRES == key_value ) //确认
		{
			while( guiInput() != 0 );
			OLED_Clear();
			(function[(list.list_option_tag+1)%list.list_num])(); //进入相应的函数
		}
		delay_ms(30);
	}
}

//仪表
void guiDashboard(void)
{
	u8 i;
//	u16 temp;
	
	while(1)
	{
		OLED_Clear();
		OLED_ShowString(30,1,"accelerator",12,1);
		OLED_DrawBoxC(0,0,128,16,5,0,1);
		
		OLED_ShowChar(0,16,'r',12,1);
		OLED_ShowChar(0,28,'y',12,1);
		OLED_ShowChar(0,40,'r',12,1);
		OLED_ShowChar(0,52,'h',12,1);

		OLED_ShowNum(10,16,ADC_Buffer[0],4,12,1);
		OLED_ShowNum(10,28,ADC_Buffer[1],4,12,1);
		OLED_ShowNum(10,40,ADC_Buffer[2],4,12,1);
		OLED_ShowNum(10,52,ADC_Buffer[3],4,12,1);
		
		for(i=0;i<4;i++)
		{	
			OLED_DrawBoxR(40,16,88*ADC_Buffer[0]/4095,10,1,1);
			OLED_DrawBoxR(40,28,88*ADC_Buffer[1]/4095,10,1,1);
			OLED_DrawBoxR(40,40,88*ADC_Buffer[2]/4095,10,1,1);
			OLED_DrawBoxR(40,52,88*ADC_Buffer[3]/4095,10,1,1);		
		}
		OLED_Refresh();
		
		u8 key_value;
		key_value = guiInput();
		if( KEY_L_PRES == key_value || KEY_R_PRES == key_value )
		{
			while( guiInput() != 0 );
			OLED_Clear();
			return;
		}
	}
}


//PID菜单
void guiPIDMenu(void)
{
	u8 pid_menu_option[][2] = {"P","Y","R","H"};
	GUI_LIST_TYPE list;
	list.list_num = 4;
	list.list_option = pid_menu_option[0];
	list.list_option_len = 2;
	list.list_option_tag = 0;
	list.list_title = "PID menu";
	list.list_title_offset = 44;

	guiListShowPrevious(&list);
	
	u8 cursor = 0;
	u8 key_value;
	
	while(1)
	{
		guiListShowNow(&list);
		
		key_value = guiInput();
		if( KEY_UP_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowPrevious(&list);
		}
		else if( KEY_DOWN_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowNext(&list);
		}
		else if( KEY_LEFT_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowPrevious(&list);
		}
		else if( KEY_RIGHT_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowNext(&list);
		}
		else if( KEY_R_PRES == key_value ) //退出
		{
			OLED_Clear();
			return;
		}
		else if( KEY_L_PRES == key_value ) //确认具体调参界面
		{
			while( guiInput() != 0 );
			OLED_Clear();
			guiPIDSet((list.list_option_tag+1)%4);
		}
		OLED_Refresh();
		cursor %= 4;
		delay_ms(30);
	}	
}

//PID调参
void guiPIDSet(u8 _cursor)
{	
	u8 key_value;
	u8 cursor = 0;
	u16 pid[3]={1234,5678,8765};

	while(1)
	{
		OLED_Clear();
	
		if( 0 == _cursor )
			OLED_ShowString(40,2,"P PID set",12,1);
		else if( 1 == _cursor )
			OLED_ShowString(40,2,"Y PID set",12,1);
		else if( 2 == _cursor )
			OLED_ShowString(40,2,"R PID set",12,1);
		else if( 3 == _cursor )
			OLED_ShowString(40,2,"H PID set",12,1);

		OLED_DrawBoxC(0,0,128,16,5,0,1);
		OLED_ShowChar(0,16,'P',16,1);
		OLED_ShowChar(0,32,'I',16,1);
		OLED_ShowChar(0,48,'D',16,1);

		
		OLED_ShowNum(16,16,pid[0],4,16,1<<cursor&0xFE);
		OLED_ShowNum(16,32,pid[1],4,16,1<<cursor&0xFD);
		OLED_ShowNum(16,48,pid[2],4,16,1<<cursor&0xFB);
		
		
		key_value = guiInput();
		if( KEY_UP_PRES == key_value )
		{
			while( guiInput() != 0 );
			cursor--;
		}
		else if( KEY_DOWN_PRES == key_value )
		{
			while( guiInput() != 0 );
			cursor++;
		}
		else if( KEY_LEFT_PRES == key_value )
		{
			pid[cursor]--;
		}
		else if( KEY_RIGHT_PRES == key_value )
		{
			pid[cursor]++;
		}
		else if( KEY_R_PRES == key_value ) //退出
		{
			while( guiInput() != 0 );
			OLED_Clear();
			return;
		}
		else if( KEY_L_PRES == key_value ) //应用设置
		{
			OLED_Clear();
			OLED_ShowString(21,19,"SUCCESS",24,1);
			OLED_Refresh();
			delay_ms(500);
		}
		OLED_Refresh();
		cursor %= 3;
		delay_ms(30);
	}
}

//设置
void guiSetting(void)
{
	u8 list_name[][8] = {"LED","Beep","Display",};
	void (*function[5])(void) = {guiLEDSet,guiBeepSet,guiDisplaySet}; //用于存放函数指针

	GUI_LIST_TYPE list;
	list.list_num = 3;
	list.list_option = list_name[0];
	list.list_option_len = 8;
	list.list_option_tag = 0;
	list.list_title = "setting";
	list.list_title_offset = 42;
	
	guiListShowPrevious(&list);
	
	u8 key_value;
	while(1)
	{
		guiListShowNow(&list);
		
		key_value = guiInput();
		if( KEY_UP_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowPrevious(&list);
		}
		else if( KEY_DOWN_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowNext(&list);
		}
		else if( KEY_LEFT_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowPrevious(&list);
		}
		else if( KEY_RIGHT_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowNext(&list);
		}
		else if( KEY_R_PRES == key_value ) //退出
		{
			OLED_Clear();
			return;
		}
		else if( KEY_L_PRES == key_value ) //确认
		{
			while( guiInput() != 0 );
			OLED_Clear();
			(function[(list.list_option_tag+1)%list.list_num])(); //进入相应的函数
		}
		delay_ms(30);
	}	
}


void guiLEDSet(void)
{
	
}
void guiBeepSet(void)
{
	
}

//显示设置
void guiDisplaySet(void)
{
	u8 list_name[][6] = {"Color","Light",};
//	void (*function[])(void) = {guiColorSet,guiLightSet}; //用于存放函数指针

	GUI_LIST_TYPE list;
	list.list_num = 2;
	list.list_option = list_name[0];
	list.list_option_len = 6;
	list.list_option_tag = 0;
	list.list_title = "Display set";
	list.list_title_offset = 42;
	
	guiListShowPrevious(&list);
	
	u8 key_value;
	while(1)
	{
		guiListShowNow(&list);
		
		key_value = guiInput();
		if( KEY_UP_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowPrevious(&list);
		}
		else if( KEY_DOWN_PRES == key_value )
		{
			while( guiInput() != 0 );
			guiListShowNext(&list);
		}
		else if( KEY_LEFT_PRES == key_value )
		{
			while( guiInput() != 0 );
			if( 0 ==  (list.list_option_tag+1)%list.list_num )
			{
				OLED_ColorTurn(1);
			}
			else if( 1 == (list.list_option_tag+1)%list.list_num )
			{
				
			}
		}
		else if( KEY_RIGHT_PRES == key_value )
		{
			while( guiInput() != 0 );
			if( 0 == (list.list_option_tag+1)%list.list_num )
			{
				OLED_ColorTurn(0);
			}
			else if( 1 == (list.list_option_tag+1)%list.list_num )
			{
				
			}
		}
		else if( KEY_R_PRES == key_value ) //退出
		{
			while( guiInput() != 0 );
			OLED_Clear();
			return;
		}
		else if( KEY_L_PRES == key_value ) //确认
		{
			while( guiInput() != 0 );
//			(function[(list.list_option_tag+1)%list.list_num])(); //进入相应的函数
		}
		delay_ms(30);
	}
}

u8 OLED_COLOR_FLAG1 = 0;
void guiColorSet(void)
{
	u8 key_value;
	while(1)
	{
		key_value = guiInput();
		if( KEY_UP_PRES == key_value )
		{
			while( guiInput() != 0 );
			return;
		}
		else if( KEY_DOWN_PRES == key_value )
		{
			while( guiInput() != 0 );
			return;
		}
		else if( KEY_LEFT_PRES == key_value )
		{
			while( guiInput() != 0 );
			OLED_ColorTurn(1);
		}
		else if( KEY_RIGHT_PRES == key_value )
		{
			while( guiInput() != 0 );
			OLED_ColorTurn(0);
		}
		else if( KEY_R_PRES == key_value ) //退出
		{
			OLED_Clear();
			return;
		}
		else if( KEY_L_PRES == key_value ) //确认
		{
			while( guiInput() != 0 );	
		}
		delay_ms(30);
	}
}

//亮度设置
void guiLightSet(void)
{
	
}



//X方向的进度条
//sch:进度，0-100
void guiProgressBarX(u8 x,u8 y,u8 dx,u8 dy,u8 sch)
{
	//8,12,16,24
	u8 char_size;
	OLED_DrawBoxC(x,y,dx,dy,dy/2,0,1);
	
	//确认字体大小
	if( dy / 24 )
		char_size = 24;
	else if( dy / 16 )
		char_size = 16;
	else if( dy / 12 )
		char_size = 12;
	else if( dy / 8 )
		char_size = 8;
}


//Y方向的进度条
void guiProgressBarY(u8 x,u8 y,u8 dx,u8 dy)
{
	
}


//GUI输入（上下左右）
u8 guiInput(void)
{	
	return key_scan();
}



