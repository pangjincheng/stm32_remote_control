#ifndef __GUI_H
#define __GUI_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "oled.h"
#include "font.h"

typedef struct _GUI_LIST_TYPE
{
	u8 *list_title; //标题
	u8 list_title_offset; //标题偏移（开始位置）
	u8 list_num;   //数目
	u8 *list_option; //名称数组指针
	u8 list_option_len; //名称数组固定长度
	u8 list_option_tag;   //用于显示的第一个名字
}GUI_LIST_TYPE;


//主菜单
void guiMenu(void);
void guiDashboard(void);
void guiPIDMenu(void);
void guiPIDSet(u8 cursor);
void guiSetting(void);
void guiLEDSet(void);
void guiBeepSet(void);
void guiDisplaySet(void);
void guiLightSet(void);
u8 guiInput(void);










#endif
