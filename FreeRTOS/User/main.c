/*
SPI1:
SPI1_NSS  - PA4  //spi片选，未使用
SPI1_SCK  - PA5
SPI1_MISO - PA6
SPI1_MOSI - PA7



*/




#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "oled.h"
#include "font.h"
#include "adc.h"
#include "gui.h"
#include "key.h"
#include "led.h"
#include "beep.h"

#define START_TASK_PRIO 1
#define START_TASK_SIZE 128
xTaskHandle START_TaskHandle;
void start_task(void *pvParameters);

xTaskHandle START1_TaskHandle;
void start1_task(void *pvParameters);
//用户交互
#define GUI_TASK_PRIO 2
#define GUI_TASK_SIZE 128
xTaskHandle GUI_TaskHandle;
void gui_task(void *pvParameters);
//守护任务，用于定时发送数据，并检测信号是否丢失
#define GUARD_TASK_PRIO 2
#define GUARD_TASK_SIZE 128
xTaskHandle GUARD_TaskHandle;
void guard_task(void *pvParameters);

extern u16 ADC_Buffer[4];


	
int main ()
{

	delay_init();
	uart_init(115200);
	OLED_Init();
	OLED_ColorTurn(0);//0正常显示，1 反色显示
	OLED_DisplayTurn(0);//0正常显示 1 屏幕翻转显示
	adc_init();
	key_init();
	led_init();
	beep_init();
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	
	printf("start\r\n");


	xTaskCreate((TaskFunction_t )start_task,
				(const char*    )"start_task",
				(uint16_t       )START_TASK_SIZE,
				(void*          )NULL,
				(UBaseType_t    )START_TASK_PRIO,
				(TaskHandle_t*  )&START_TaskHandle);

	vTaskStartScheduler();

}




void start_task(void *pvParameters)
{
	printf("start_task\r\n");
	taskENTER_CRITICAL();
	
	/*
	//创建消息队列
	NRF24L01_QueueHandle = xQueueCreate(NRF24L01_Q_NUM,sizeof(u8));

	xTaskCreate((TaskFunction_t )nrf24l01_send_task,
				(const char*    )"nrf24l01_send_task",
				(uint16_t       )NRF24L01_SEND_TASK_SIZE,
				(void*          )NULL,
				(UBaseType_t    )START_TASK_PRIO,
				(TaskHandle_t*  )&NRF24L01_SEND_TaskHandle);	
	*/
		
	//测试任务			
	xTaskCreate((TaskFunction_t )start1_task,
				(const char*    )"start1_task",
				(uint16_t       )128,
				(void*          )NULL,
				(UBaseType_t    )5,
				(TaskHandle_t*  )&START1_TaskHandle);
	//用户交互任务
	xTaskCreate((TaskFunction_t )gui_task,
				(const char*    )"gui_task",
				(uint16_t       )GUI_TASK_SIZE,
				(void*          )NULL,
				(UBaseType_t    )GUI_TASK_PRIO,
				(TaskHandle_t*  )&GUI_TaskHandle);
	//守护任务，用于定时发送数据，并检测信号是否丢失
	xTaskCreate((TaskFunction_t )guard_task,
				(const char*    )"guard_task",
				(uint16_t       )GUARD_TASK_SIZE,
				(void*          )NULL,
				(UBaseType_t    )GUARD_TASK_PRIO,
				(TaskHandle_t*  )&GUARD_TaskHandle);


	
	vTaskDelete(START_TaskHandle);
				
	taskEXIT_CRITICAL();
}


void start1_task(void *pvParameters)
{
	printf("start1_task\r\n");
	while(1)
	{
		printf("runing\r\n");
		vTaskDelay(10);
	}
}

//extern void guiMenu(void);
//用户交互任务
void gui_task(void *pvParameters)
{
	guiMenu();
	
	while(1)
	{
//		guiDashboard();
		vTaskDelay(30);		
	}
}

//守护
void guard_task(void *pvParameters)
{
	while(1)
	{
		delay_ms(10);
	}
}
